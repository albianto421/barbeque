#ifndef FORTRAN_EXC_H_
#define FORTRAN_EXC_H_

#include <bbque/bbque_exc.h>

extern "C" int bbqonsetup_();
extern "C" int bbqonconfigure_();
extern "C" int bbqonrun_();
extern "C" int bbqonmonitor_();
extern "C" int bbqonrelease_();

#define Fortran_OnSetup bbqonsetup_
#define Fortran_OnConfigure bbqonconfigure_
#define Fortran_OnRun bbqonrun_
#define Fortran_OnMonitor bbqonmonitor_
#define Fortran_OnRelease bbqonrelease_

using bbque::rtlib::BbqueEXC;

class FortranWrapper : public BbqueEXC {

public:

	FortranWrapper(std::string const & name,
			std::string const & recipe,
			RTLIB_Services_t *rtlib);

private:

	RTLIB_ExitCode_t onSetup();
	RTLIB_ExitCode_t onConfigure(int8_t awm_id);
	RTLIB_ExitCode_t onRun();
	RTLIB_ExitCode_t onMonitor();
	RTLIB_ExitCode_t onSuspend();
	RTLIB_ExitCode_t onRelease();
};

#endif 
